#reset
#set terminal postscript eps color solid enhanced 20
#set terminal postscript eps monochrome 24
#set output "step.eps"
#set key default
#set key out horiz
#set key top center

#set grid y
#set style line 1  lt -1 lw 4 pt 2 ps 1
#set style line 2  lt -1 lw 4 pt 4 ps 1
#set style line 3  lt -1 lw 4 pt 8 ps 1
#set style line 4  lt 0 lw 4 pt 4 ps 1
#set style line 5  lt 0 lw 4 pt 8 ps 1
#set boxwidth 1 absolute
#set style data histogram
#set style histogram cluster gap 3
#set style fill solid
#set ylabel "#Execution Time (s)"
#set xlabel "#Feature Space Size"


#plot 'steppercent.dat' using 2:xtic(1) title "S1" fill pattern 4 lc rgb "#009E73", '' using 3 title "S2" fill pattern 5 lc rgb "#56B4E9", '' using 4 title "S3" fill pattern 2 lc rgb "#CC79A7"



#file1 = 'steppercent.dat'

reset
set terminal postscript eps color solid enhanced 24
set terminal postscript eps monochrome 24
set output "step.eps"
set style data histogram
set style histogram rowstack
set style fill pattern


#set xtics border in scale 0, 0 nomirror rotate by 90 offset character 0, -6, 0
set boxwidth 0.69 absolute

set yrange [0:100]

set key out horiz
set key top center
#set key outside right top vertical left
set key samplen 2.5 spacing 0.85

set ylabel "ALS Steps BreakDown (%)" font ",18" offset character 2.5,0,0

plot 'steppercent.dat' using 2:xtic(1) title "S1" fill pattern 2 lc rgb "#CC79A7", '' using 3 title "S2" fill pattern 5 lc rgb "#56B4E9", '' using 4 title "S3" fill pattern 4 lc rgb "#009E73"
