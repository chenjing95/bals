reset
set terminal postscript eps color solid enhanced 20
#set terminal postscript eps monochrome 24
set output "com_yhr1.eps"
set key default
set key out horiz
set key top center

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 3
set style fill solid
set ylabel "#GFlops"
set xlabel "#Feature Space Size (ML20M)"

plot 'results_SAC_HPDC_Bigdata.dat' using 11:xtic(1) title "Rodrigues" fill pattern 4 lc rgb "#009E73", '' using 7 title "CuMF" fill pattern 5 lc rgb "#56B4E9", '' using 3 title "Gates" fill pattern 2 lc rgb "#CC79A7"
