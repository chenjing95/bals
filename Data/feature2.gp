reset
set terminal postscript eps color solid enhanced 20
#set terminal postscript eps monochrome 24
set output "feature2.eps"
set key default
set key out horiz
set key top center

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 3
set style fill solid
#set ytics 200
set ylabel "#Gflops"
set xtic rotate by 0 scale 0
set xlabel "#Feature Space Size"


set ytics nomirror
#set y2tics
set tics out
#set autoscale y2
#set y2range [0:*]

plot 'featuresize.dat' using 2:xtic(1) title "BALS-NTFX" fill pattern 4 lc rgb "#D55E00", '' using 3 title "Gates-NTFX" fill pattern 5 lc rgb "#56B4E9", '' using 8 title "BALS-YHR1" fill pattern 1 lc rgb "#CC79A7", '' using 9 title "Gates-YHR1" fill pattern 1 lc rgb "#009E73"
