reset
set term postscript eps color solid 18
set output "featuresize.eps"
set grid
set key top left
#set key outside
set xlabel "#Feature Space Size"
#set yrange [0:*]
set ylabel "#GFlops"
#set xtic rotate by -45 scale 0

set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"
set style line 5  lt 5 lw 4 pt 2 ps 2 lc rgb "#999999"
set style line 6  lt 6 lw 4 pt 10 ps 2 lc rgb "#D54E00"


plot "featuresize.dat" using 2:xtic(1) title 'BALS_NTFX' with linespoints ls 1, '' using 3:xtic(1) title 'Bigdata_NTFX' with linespoints ls 2,'' using 4:xtic(1) title 'BALS_ML20M' with linespoints ls 3,'' using 5:xtic(1) title 'Bigdata_ML20M' with linespoints ls 4,'' using 6:xtic(1) title 'BALS_ML10M' with linespoints ls 5,'' using 7:xtic(1) title 'Bigdata_ML10M' with linespoints ls 6, '' using 8:xtic(1) title 'BALS_YHR1' with linespoints ls 7,'' using 9:xtic(1) title 'Bigdata_YHR1' with linespoints ls 8
