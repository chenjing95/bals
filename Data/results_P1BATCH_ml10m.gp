reset
set term postscript eps color solid 18
set output "results_P1BATCH_ml10m.eps"
set grid
#set key top left
set key outside top horizon
#set key outside right
set xlabel "#DR Batch Number"
#set yrange [0.03:*]
set ylabel "Execution time [s]"


set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"
set style line 5  lt 5 lw 4 pt 2 ps 2 lc rgb "#999999"
set style line 6  lt 6 lw 4 pt 10 ps 2 lc rgb "#D54E00"
set style line 7  lt 7 lw 4 pt 14 ps 2 lc rgb "#E14900"
set style line 8  lt 8 lw 4 pt 18 ps 2 lc rgb "#C3EE00"

#set log y 2
#set format y '2^{%L}'; 
#set ytics (64, 32, 8, 2, 0.5, 0.125, \
#	0.03125, 0.0078125, 0.001953125)



plot "results_P1BATCH_ml10m.dat" using 2:xtic(1) title 'Gates-ML10M' with linespoints ls 1, "results_P1BATCH_ml20m.dat" using 2:xtic(1) title 'Gates-ML20M' with linespoints ls 2, "results_P1BATCH_ml10m.dat" using 4:xtic(1) title 'Gates_Re-ML10M' with linespoints ls 3, "results_P1BATCH_ml20m.dat" using 6:xtic(1) title 'Gates_Re-ML20M' with linespoints ls 4, "results_P1BATCH_ml10m.dat" using 3:xtic(1) title 'BALS-ML10M' with linespoints ls 5, "results_P1BATCH_ml20m.dat" using 4:xtic(1) title 'BALS-ML20M' with linespoints ls 6, "results_P1BATCH_ml10m.dat" using 5:xtic(1) title 'BALS_Re-ML10M' with linespoints ls 7, "results_P1BATCH_ml20m.dat" using 8:xtic(1) title 'BALS_Re-ML20M' with linespoints ls 8
