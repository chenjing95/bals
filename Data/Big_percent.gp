reset
set terminal postscript eps color solid enhanced 24
set terminal postscript eps monochrome 24
set output "Big_percent.eps"
set style data histogram
set style histogram rowstack
set style fill pattern


#set xtics border in scale 0, 0 nomirror rotate by 90 offset character 0, -6, 0
set boxwidth 0.55 absolute

set yrange [0:100]

set key out horiz
set key top center
#set key outside right top vertical left
set key samplen 2.5 spacing 0.85

set ylabel "Step1 BreakDown (%)" font ",18" offset character 2.5,0,0

plot 'Big_percent.dat' using 2:xtic(1) title "Load" fill pattern 4 lc rgb "#D55E00", '' using 3 title "Calculate" fill pattern 5 lc rgb "#56B4E9", '' using 4 title "Save" fill pattern 1 lc rgb "#CC79A7"
