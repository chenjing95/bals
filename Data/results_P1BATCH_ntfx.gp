reset
set term postscript eps color solid 18
set output "results_P1BATCH_ntfx.eps"
set grid
#set key top left
set key outside top horizon
set xlabel "#DR Batch Number"
#set yrange [0:*]
#set logscale y 10
set ylabel "Execution time [s]"


set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"
set style line 5  lt 5 lw 4 pt 2 ps 2 lc rgb "#999999"
set style line 6  lt 6 lw 4 pt 10 ps 2 lc rgb "#D54E00"
set style line 7  lt 7 lw 4 pt 14 ps 2 lc rgb "#E14900"
set style line 8  lt 8 lw 4 pt 18 ps 2 lc rgb "#C3EE00"

plot "results_P1BATCH_ntfx.dat" using 2:xtic(1) title 'Gates-NTFX' with linespoints ls 1, "results_P1BATCH_yhr1.dat" using 2:xtic(1) title 'Gates-YHR1' with linespoints ls 2, "results_P1BATCH_ntfx.dat" using 4:xtic(1) title 'Gates_Re-NTFX' with linespoints ls 3, "results_P1BATCH_yhr1.dat" using 4:xtic(1) title 'Gates_Re-YHR1' with linespoints ls 4, "results_P1BATCH_ntfx.dat" using 3:xtic(1) title 'BALS-NTFX' with linespoints ls 5, "results_P1BATCH_yhr1.dat" using 3:xtic(1) title 'BALS-YHR1' with linespoints ls 6, "results_P1BATCH_ntfx.dat" using 5:xtic(1) title 'BALS_Re-NTFX' with linespoints ls 7, "results_P1BATCH_yhr1.dat" using 5:xtic(1) title 'BALS_Re-YHR1' with linespoints ls 8
