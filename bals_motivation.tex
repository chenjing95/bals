\section{Motivation} \label{sec_bals_motivation}
\subsection{Observation}
According to our statistics on various recommendation datasets, we observe that 
%the majority of users has a rating to a small proportion of items, which are more prevalent than others. 
\emph{there exist popular items which have been rated by the majority of users and, at the same time, there exist users who have given a score to most, if not all, items}.
%For instance, in Netflix \% users approximately give a mark to top 20\% popular movies and \% users give a mark to \% prevalent musics in Yahoomusic R1.
To visualize this trend, we organize the rows of the \texttt{Netflix} dataset in the form of row blocks, 
and count the number of ratings for each movie and within a row block.
%That is, we partition users into distinct groups. 
%Figure~\ref{fig_column_reuse} shows the heatmaps about total reuse times of each column in a row block over \texttt{Netflix} dataset before and after data reordering.
%Figure~\ref{fig_column_reuse} shows the heatmaps about the number of users who have rated the same items in a row block. 
We see from Figure~\ref{fig_ori} that 
some movies are watched by an enormous user group (many ratings in one column),
 while others by only a few (very few ratings in one column).
This results in a structure of vertical lines in dark red. 

%The results enlighten us that we could exploit the statistics feature that a large amount of users give a mark to a a small proportion of popular items.
%has much columns data reuse and batching strategy of \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} for others.
In alternating least squares, updating a user vector ($\mathbf{u}_{i}$) needs to load the data elements (i.e., $nnz_{i}$ column vectors each sized of $f$) from $\mathbf{Y}$ 
according to the column indexes of the items that the user have rated.
When updating a neighbouring user vector ($\mathbf{u}_{i+1}$), the same column vectors of $\mathbf{Y}$ can be used again. 
This occurs when two non-zero entries from different rows of $\mathbf{R}$ share the same column index. 
Thus, we can avoid a movement of the column vectors in $\mathbf{Y}$, so as to save the memory bandwidth. 
%and utimately improve the overall factorization performance.
Our observation has shown that this is a common case in recommendation datasets.
However, the previous implementations let different threads (or thread blocks) work on distinct user vectors~\cite{DBLP:conf/bigdataconf/GatesAKD15, DBLP:conf/hpdc/TanCF16}.
The already-loaded column vectors from $\mathbf{Y}$ are either evited out of caches or be manually overwritten in scratch-pad memories. 
This is the same when we calculate item vectors. 
In this paper, we aim to exploit such a data feature to avoid redundant data movement and enhance data reuse.

%The previous implementations run the updating work independently, which ignore the data features of dataset statistics, cause the enormous data loading and decline the performance as well.
%However, if the obtained statistics are exploited and users are clustered into a group according to the ratings to same items, we could use \textit{data reuse} and decrease the amount of data loading to update all users/items.

\begin{figure}[!t]
\centering
\subfigure[Original \texttt{Netflix}]{\label{fig_ori}\includegraphics[width=0.49\textwidth]{./dia/ori2048_netflix.png}}
\subfigure[Reordering \texttt{Netflix}]{\label{fig_re}\includegraphics[width=0.49\textwidth]{./dia/re_2048netflix.png}}
\caption{
%The heatmaps of column reuse times about each row block over Netflix dataset before and after data reordering, where the row block size equals 2048.
The illustration about movies popularity of user groups over \texttt{Netflix} dataset.}
%\textcolor{cyan}{Fixme: we have to use a better name than "reuse". In general, we can use 'popularity' to denote how much a user likes a movie. Anyway, we should avoid the technical terms to appear early in the context.} 
\label{fig_column_reuse}
\end{figure}

\subsection{Data Reuse}
If there exist two nonzero elements of distinct rows sharing the same column index, 
updating the corresponding row vectors requires the same column vector from $\mathbf{Y}$.
%we will load the same column vector from $\mathbf{Y}$.
Actually, we can avoid one extra data movement of $f$ data elements when updating the row vectors simultaneously. 
We regard this as a \textit{data reuse}.
%In \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15}, the repeated loads present a pressure for memory bandwith and lead to intensive memory accesses. 
%, we can avoid one-time loading of $f$ data elements once the following equation holds.
Analytically, a data reuse occurs once Equation~\ref{eq:data_reuse} holds.

\begin{equation}
\label{eq:data_reuse}
col\_idx[row\_ptr[start_{a}+i]]==col\_idx[row\_ptr[start_{b}+j]],
\end{equation}
where $a$ and $b$ are two adjacent users in $\mathbf{R}$, $i$ denotes the $i^{th}$ nonzero element of row $a$ and $j$ denotes the $j^{th}$ nonzero element of row $b$.
Figure~\ref{fig_Data_reuse} (left) illustrates the concept of \textit{data reuse}, 
where we group 6 rows into 2 row blocks each with 3 rows.
% (R0, R1, R2; R3, R4, R5). 
%and each rowblock contains three rows.
The first non-zero element of R0 has the same column index (C0) as the first non-element of R1.
% which denotes a time of data reuse.
Thus, we only have to load the column vector once (vector 1 shaded in yellow). 
In the same way, the column vectors (vector 4 and 5) can be reused in the second row block.
%Therefore, the loading tasks of the matrix only need to move data size of 8$\times$f instead of 11$\times$f.
%In summary, the observations motivte us to investigate how to take the advantage of \textit{data reuse} for a minimization of duplicated data movements.

\begin{figure}[!t]
\centering
\includegraphics[width=0.90\textwidth]{./dia/Data_reuse.eps}
\caption{The illustration of data reuse and the comparison before and after data reordering, 
where the rowblock size of the matrix is 3. Matrix $\mathbf{Y}$ denotes the collection of item vectors and $\mathbf{sY}$ is the cached content used to update each user vector.}
\label{fig_Data_reuse}
\end{figure}


\subsection{Motivation}
As we have shown, computing $\mathbf{Y}^{T}\mathbf{Y}$ takes more than 90\% of the entire factorization.
While taking a closer look, we know calculating $\mathbf{Y}^{T}\mathbf{Y}$, again, consists of three steps: 
(1) loading data into shared memory, 
(2) calculating $\mathbf{sY}^{T}\mathbf{sY}$, 
(3) storing results back to global memory.
Figure~\ref{fig_Big_percent} shows the breakdown of the three steps.
We note that the data loading process (step 1) takes around 50\% of calculating $\mathbf{Y}^{T}\mathbf{Y}$,
while the remaining two steps consume the other half. 
By reducing the data loading time, we can efficiently speed up the entire factorization process. 
This motivates us to investigate the exploition of \textit{data reuse} with an aim to minimizing duplicated data movements. 


%Meanwhile, 
%we observe that the most time-consuming step of \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} is mainly about loading Y columns from global memory to shared memory sY (step 1) as Figure~\ref{fig_Big_percent} shows. 
%In \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15}, they exploit (f/nb)$ \times $(f/nb) thread blocks to update each row of $R$ matrix and these loading tasks of updating rows are independent with each other.
%However, there exists much \textit{data reuse}.
%If there exist two nonzero elements of two adjacent rows having the same column index, the updating task will save a time of column loads.  
%We regard this as a time of \textit{data reuse}.
%That is, we can avoid an extra loading of $f$ data elements when the following equation holds. 
%\begin{equation}
%col\_idx[row\_ptr[start_{a}+i]]==col\_idx[row\_ptr[start_{b}+j]],
%\end{equation}
%where $a$ and $b$ are two adjacent rows in $R$ matrix, $i$ denotes the $i^{th}$ nonzero element of $a$ row and $j$ denotes the $j^{th}$ nonzero element of $b$ row.
%Figure~\ref{fig_Data_reuse} is a example about what is a time of \textit{data reuse}. 
%For instance, we arrange rows into row blocks and each rowblock contains three rows.
%The first non-zero element of Row 0 has the same column index with the first non-element of Row 1, which denotes a time of data reuse.
%In the same way, we can find that there exit two times of data reuse in the second row block.
%Therefore, the loading tasks of the matrix only need to move data size of 8$\times$f instead of 11$\times$f.
%In summary, the observations motivte us to investigate how to take the advantage of \textit{data reuse} for a minimization of duplicated data movements. 


%\begin{figure}[!t]
%\centering
%\includegraphics[width=0.60\textwidth]{./Data/Big_percent.eps}
%\caption{The percentage of three steps in \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} over various datasets. }
%\label{fig_Big_percent}
%\end{figure}

It is a common observation that most elements of sparse recommendation matrix are zeros. 
%has more blank fragments due to the sparsity of data distribution. 
Dealing with these zero segments takes a decent amount of the factorization time.
% and declines the performance.
%If we could cluster most of nonzero elements of sparse matrix, the performance would improve dramatically.
%Therefore, 
For this, we propose a \textit{data reordering} technique to further enhance data locality.
% and maximize the performance benefits. 
This can be achieved by sorting the rows or columns of $\mathbf{R}$ in the decending order of non-zeros. 
%according to nonzero elements of rows/columns. 
%In this way, 
By doing so, most of nonzero elements move towards the top-left corner of the matrix (Figure~\ref{fig_Data_reuse}).
We argue that reordering rows or columns can further decrease the data movements and improve data reuse.
% which can avoid much computing cost of blank tile and blank rows/columns. 
%shown in Figure~\ref{fig_Data_reuse}, the majority of non-zero elements cluster at top left corner of matrix and increase the times of \textit{data reuse}. 
%Meanwhile, we count the number of ratings for each item within a row block, after \textit{data reordering} (Figure~\ref{fig_re}).
%who have a rating to specific items after \textit{data reordering} as Figure~\ref{fig_re} shows, which has the same regularity.
Figure~\ref{fig_re} shows the rating heatmap after \textit{reordering}. 


