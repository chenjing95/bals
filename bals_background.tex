\section{Background} \label{sec_recsys_background}
In this section, we describe als-based matrix factorization and then introduce three baseline implementations in CUDA.

\subsection{ALS-based Matrix Factorization}
\textit{Matrix factorization (or completion)} is the task of filling in the missing entries of a partially observed matrix.
The input of matrix factorization is an incomplete relation matrix between users and items, $R(m \times n)$, 
where $m$ denotes the number of users and $n$ denotes the number of items. 
Due to the sparsity of $\mathbf{R}$, matrix factorization maps both users and items to a joint factor space of dimensionality $f$, a.k.a. \textit{latent feature}, so that predicting unknown ratings can be estimated by the inner products of two vectors, $x_{u}$ of matrix $\mathbf{X}(m \times f)$ and $y_{i}$ of matrix $\mathbf{Y}(n\times f)$, 

\begin{equation}
r_{ui}=x_{u}  {y_{i}}^{T},
\end{equation}
where $x_{u}$ denotes the extent of user's interest on items. Similarly, $y_{i}$ denotes the extent to which the item owns these factors, and $r_{ui}$ denotes an entry of the rating matrix $\mathbf{R}$. 
The key of the problem is to obtain $x_{u}$ and $y_{i}$ so that $R\approx XY^{T}$. The basic idea for matrix factorization is to minimize the regularized squared error on the observed ratings to learn the factors,

\begin{equation}
L(X, Y)=\sum_{u,i\in \Omega} (r_{ui}-x_{u}^{T}y_{i})^{2}+\lambda (|x_{u}|^{2}+|y_{i}|^{2}), 
\label{eq:obj_function}
\end{equation}
where $\Omega$ are the known nonzero ratings of $\mathbf{R}$, and $x_{u}^{T}$ are the $u^{th}$ row vectors of the matrix $\mathbf{X}$, $y_{i}$ are $i^{th}$ column vectors of matrix $\mathbf{Y}$, the constant $\lambda$ is the regularized coefficient to avoid over-fitting. Therefore, the key to solve this problem is to find approaches of getting the matrices $\mathbf{X}$ and $\mathbf{Y}$. 
%\textit{Alternating least squares} (ALS) and \textit{stochastic gradient descent} (SGD) are two typical techniques to minimize Function \ref{eq:obj_function}. 

Among all the matrix factorization techniques, \textit{alternating least squares} (ALS) is regarded as an efficient one. 
The minimization principle of alternating least squares is \textit{to keep one fixed while calculating the other}: we fix the $Y$ matrix to calculate the $\mathbf{X}$ matrix so as to get vectors $x_{u}$, and vice versa. 
In this way, the problem becomes a quadratic function. The procedure iterates until it converges. 
First, we minimize the equation over $\mathbf{X}$ while fixing $\mathbf{Y}$, and the function becomes 

\begin{equation}
L(X)=\sum_{i \in \Omega_{u}}(r_{ui}-x_{u}^{T}y_{i})^{2}+\lambda |x_{u}|^{2}
\label{eq:obj_function_x}
\end{equation}

By calculating the partial derivative of $x_{u}$ in Function~\ref{eq:obj_function_x} and letting the partial derivative equal zero, we can obtain 

\begin{equation}
x_{u}=(Y^{T}Y+\lambda I)^{-1}Y^{T}r_{u}, 
\label{eq:x_u}
\end{equation}
where $I$ is the unit matrix ranked $f$, and $r_{u}$ is the $u^{th}$ rows of $\mathbf{R}$. In the same way, we can obtain $y_{i}$

\begin{equation}
y_{i}=(X^{T}X+\lambda I)^{-1}X^{T}r_{i}.
\label{eq:y_i}
\end{equation}

\begin{figure}[!t]
\centering
\subfigure[]{\label{fig_step_ALS}\includegraphics[width=0.45\textwidth]{./Data/step.eps}}
\subfigure[]{\label{fig_Big_percent}\includegraphics[width=0.45\textwidth]{./Data/Big_percent.eps}}
\caption{The left figure illustrates the ALS step percentage on Netflix dataset running on K20C. 
%Note: step 3 takes little time of the whole algorithm (below 1\%). 
The right figure represents the percentage of three steps in calculate $\mathbf{Y}^{T}\mathbf{Y}$ of \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} over various datasets. Computing $Y^{T}Y$ consists of three steps: (1) loading data into shared memory, (2) calculating $sY^{T}*sY$, (3) storing results back to global memory. }
\label{fig_step}
\end{figure}

\subsection{Algorithm Analysis}
The algorithm consists of three steps when factorizing the rating matrix.
Taking $x_{u}$ as a example, the three steps are (S1) $Y^{T}Y+\lambda I$, (S2) $Y^{T}r_{u}$, and (S3) solving the linear system. 
As for S1, calculating $Y^{T}Y$ requires $nnz_{i}\times f\times(f+1)/2$ \textit{multiply-add} operations for a row of $\mathbf{R}$, 
where $nnz_{i}$ denotes the number of nonzero entries in the current row. 
Therefore, the total computing cost is $nnz\times f\times (f+1)$, 
where $nnz$ denotes the total number of non-zero elements in $\mathbf{R}$. 
In terms of memory footprint, we need a matrix $smat$ sized of $f \times f$ to store the results of $\mathbf{Y}^{T}\mathbf{Y}$ in global memory when updating a row. Thus, the total memory footprint for $m$ rows is $m\times f \times f$.
Calculating S2 requires $nnz_{i} \times f$ \textit{multiply-add} operations when updating the $i^{th}$ row of $\mathbf{R}$. 
Thus, the total computing cost of S2 is $nnz \times f \times 2$. 
This step needs a vector $svec$ sized of $f$ to store the results of $Y^{T}r_{u}$,
% in global memory when updating a row of $R$, 
and thus the total memory footprint for $m$ rows is $m\times f$.
After obtaining matrix $smat$ and vector $svec$, \textit{cholesky} decomposition method is often exploited to solve the linear system $smat \cdot x_{u}=svec$ (S3). 
This is due to the fact that $smat$ is symmetric, positive and definite. 
And the time complexity of cholesky method is O($f^{3}$) for updating a row of $\mathbf{R}$.
By running a serials of experiments, we observe that $\mathbf{Y}^{T}\mathbf{Y}$ is the most time-consuming step (90\% average as Figure~\ref{fig_step_ALS} shows), which thus becomes the tuning focus in this work. 
%\textcolor{cyan}{FIXME: it is better that Figure~\ref{fig_step_ALS} use the breakdown style plot.}

\begin{figure}[!h]
\centering
\subfigure[ML10M]{\label{fig_com_ML10M}\includegraphics[width=0.45\textwidth]{./Data/com_ml10M.eps}}
\subfigure[ML20M]{\label{fig_com_ML20M}\includegraphics[width=0.45\textwidth]{./Data/com_ml20M.eps}}
\subfigure[NTFX]{\label{fig_com_NTFX}\includegraphics[width=0.45\textwidth]{./Data/com_ntfx.eps}}
\subfigure[YHR1]{\label{fig_com_YHR1}\includegraphics[width=0.45\textwidth]{./Data/com_yhr1.eps}}
\caption{The performance comparison of three implementations on GPUs. However, the implementation of Rodrigues is rather low and can not show in this figure.}
\label{fig_results_SAC_HPDC_Bigdata}
\end{figure}

%\subsection{Baseline Implementations}
\subsection{State-of-the-Art ALS Implementations}
In this section, we describe three state-of-the-art ALS implementations and compare their performance on GPUs.

Rodrigues et al. introduce a basic ALS implementation in CUDA~\cite{DBLP:conf/sac/RodriguesJD15}, 
where each GPU thread updates a row $\mathbf{x_{u}}$ of $\mathbf{X}$ (Equation~\ref{eq:x_u}) or a column $\mathbf{y_{i}}$ of $\mathbf{Y}$ (Equation~\ref{eq:y_i}). 
Thus, the implementation has a total of $m$ (or $n$) tasks and at most $m$ (or $n$) threads can run concurrently. 
As one GPU thread is used to update a row of the $\mathbf{X}$ matrix, all the temporary data of $\mathbf{Y}^{T}\mathbf{Y}$ is allocated dynamically in the kernel function. 
However, when $f$ becomes large, there is insufficient global memory space remained for dynamic allocation and thus the kernel failed to run. 
Therefore, the implementation does not scale over the latent factor.

\texttt{CuMF} uses a thread block to update a row of the $\mathbf{X}$ matrix or a column of the $\mathbf{Y}$ matrix~\cite{DBLP:conf/hpdc/TanCF16}. 
The entire task of calculating $\mathbf{Y}^{T}\mathbf{Y}$ is partitioned into multiple tiles, each sized of $10\times 10$. 
Then \texttt{CuMF} lets each thread work on such a data tile. 
Instead of using a loop to iterate a $10\times 10$ data tile, it fully unrolls the loop and allocates 100 registers to store the temporary results of $smat$. 
Taking $f=10$ for example, \texttt{CuMF} uses only one thread to calculate the temporary results of $\mathbf{Y}^{T}\mathbf{Y}$. 
But \texttt{CuMF} exploits a special kernel for $f=100$, which cause the dramatic performance promotion than others.

Gates et al. present an ALS solver in CUDA~\cite{DBLP:conf/bigdataconf/GatesAKD15}.
%Thus, we take \texttt{Bigdata15} as our baseline implementation.
They leverage a batched implementation and use a 3D grid of thread blocks, 
($\left\lceil\frac{f}{nb}\right\rceil$, $\left\lceil\frac{f}{nb}\right\rceil$, $batch$).
%as shown Figure~\ref{fig_Bigdata_schematic}.
The product of $\mathbf{Y}^{T}\mathbf{Y}$ is a f$\times$f matrix for each row of $\mathbf{R}$, and this task %of computing $Y^{T}Y$ in \texttt{Bigdata15} 
is divided into sub-tiles sized of nb$\times$nb. 
Each sub-tile is mapped to a thread block.
%As shown in Figure~\ref{fig_Bigdata_schematic}, 
The approach moves the corresponding columns of $\mathbf{Y}$ sized of $kb \times f$ 
into shared memory $sY$ (or $sY^{T}$) and exploits (f/nb)$ \times $(f/nb) thread blocks to calculate $\mathbf{Y}^{T}\mathbf{Y}$. 
%Therefore, the computing task of each row will be handled by four thread blocks. 
Meanwhile, they use dx$\times$dy threads per thread block and allocate register files $rA$ sized of (nb/dx)$ \times $(nb/dy) for each thread to save the temporary multiplication results.
Figure~\ref{fig_results_SAC_HPDC_Bigdata} compares three state-of-the-art ALS implementations over different feature space size on four datasets.  
We can find that Gates' approach generally runs the fastest on GPUs among three implementations.
%, which is shown in Figure~\ref{fig_results_SAC_HPDC_Bigdata}.
%\textcolor{cyan}{FIXME: the figure is not clear to me.} 

