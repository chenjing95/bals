\section{Performance Evaluation} \label{sec_bals_results}
This section reports the performance evaluations of \texttt{BALS}. 
We analyse the performance impact of several parameters, including xblock, yblock, DRbat, feature space size before and after reordering and then compare \texttt{BALS} with the fastest state-of-the-art implementation \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} in different aspects.

\begin{figure}[!t]
\centering
\subfigure[datasets 1]{\label{fig_ACS_ml10M}\includegraphics[width=0.45\textwidth]{Data/ACS_ml10m.eps}}
\subfigure[datasets 2]{\label{fig_ACS_ntfx}\includegraphics[width=0.45\textwidth]{Data/ACS_ntfx.eps}} 
\caption{Implementation performance comparison of CCD++ and ALS algorithms over various latent feature size, where we both use the optimum parameters in \texttt{BALS} and \texttt{CDMF} (8192 thread blocks, 64 threads per block) in K20C.} 
\label{fig_ACS}
\end{figure}

\subsection{Performance Comparison with SGD and CCD++}
In various machine learning applications, alternating least squares algorithm has been demonstrated to be effective in solving matrix factorization.
There are also two other algorithms to solve MF, cyclic coordinate decent plus plus (CCD++) and stochastic gradient descent (SGD).
In this paper, we compare \texttt{BALS} with state-of-the-art implementations \texttt{CDMF} (CCD++) and \texttt{cuMF\_SGD} (SGD).
Since \texttt{cuMF\_SGD} only supports k=128, so we only compare them in this case and obeserve that \texttt{BALS} completely outperforms \texttt{cuMF\_SGD}. 

Figure~\ref{fig_ACS} shows the performance comparison between \texttt{CDMF} and \texttt{BALS} over different latent feature size.
\texttt{CDMF} scale linearly over f ranging from 8 to 96, while the performance of \texttt{BALS} fluctuate slightly due to the different parameters chosen for different $f$.
We can observe that \texttt{BALS} completely outperforms \texttt{CDMF} over different f and achieves 4.5 - 23.5$\times$ speedup.


\begin{figure}[!t]
\centering%\subfigure[SLS]{\label{fig_sls}\includegraphics[width=0.32\textwidth]{Data/sls_ori.eps}}
%\subfigure[RUCI]{\label{fig_rucci}\includegraphics[width=0.32\textwidth]{Data/rucci_ori.eps}}
\subfigure[ML10M(K20C)]{\label{fig_ml10M}\includegraphics[width=0.32\textwidth]{Data/ml10m_ori.eps}}
\subfigure[ML20M(K20C)]{\label{fig_ml20M}\includegraphics[width=0.32\textwidth]{Data/ml20m_ori.eps}} 
\subfigure[NTFX(K20C)]{\label{fig_ntfx}\includegraphics[width=0.32\textwidth]{Data/ntfx_ori.eps}} 
\subfigure[YHR1(K20C)]{\label{fig_yhR1}\includegraphics[width=0.32\textwidth]{Data/yhr1_ori.eps}}
\subfigure[SLS(K20C)]{\label{fig_sls}\includegraphics[width=0.32\textwidth]{Data/sls_ori.eps}}
\subfigure[RUCI(K20C)]{\label{fig_rucci}\includegraphics[width=0.32\textwidth]{Data/rucci_ori.eps}}
%\subfigure[12MT(K20C)]{\label{fig_12month}\includegraphics[width=0.32\textwidth]{Data/12month_ori.eps}}
\subfigure[ML10M(TITAN X)]{\label{fig_ml10M_p}\includegraphics[width=0.32\textwidth]{Data/ml10m_paskal.eps}}
\subfigure[ML20M(TITAN X)]{\label{fig_ml20M_p}\includegraphics[width=0.32\textwidth]{Data/ml20m_pascal.eps}}
\subfigure[NTFX(TITAN X)]{\label{fig_ntfx_p}\includegraphics[width=0.32\textwidth]{Data/ntfx_pascal.eps}}
\subfigure[YHR1(TITAN X)]{\label{fig_yhR1_p}\includegraphics[width=0.32\textwidth]{Data/yhr1_pascal.eps}}
\subfigure[SLS(TITAN X)]{\label{fig_sls_p}\includegraphics[width=0.32\textwidth]{Data/sls_pascal.eps}}
\subfigure[RUCI(TITAN X)]{\label{fig_rucci_p}\includegraphics[width=0.32\textwidth]{Data/rucci_pascal.eps}}
\caption{The performance impact over xblock and yblock and performance comparison of \texttt{BALS} and \texttt{Gates} before data reordering on various datasets, where $f$=32, nb=16, kb=8, DRbat=1.} 
\label{fig_results_xblock}
\end{figure}

\subsection{Performance Impact of Xblock and Yblock}
In \texttt{BALS}, xblock denotes the row block size of $R$ matrix and it directly affects total column reuse times not only in a tile but also in the whole row block. 
Yblock denotes the column block size of $R$ matrix and it has an effect on column reuse times of a tile.
In other words, when the yblock of a tile is fixed, column reuse times of the tile will increase with xblock increasing theoretically and vice versa.
The total column reuse times in a entire row block is only related with xblock. 
Taking $f$=32 as example, 384 is the maximum number of yblock on Tasla K20C, which has 48KB shared memory in total.
But through a large number of experiments, we find that the performance of \texttt{BALS} do not reach its lowest point when yblock=384.
Therefore, there is still some space for performance improvement if devices have more shared memory.
%In addtion, batch denotes the total number of rows in an iteration of \texttt{BALS} and it should be integer multiples of xblock. 
%For example, Batch=12X in Figure~\ref{fig_ntfx} denotes that there are 12 row blocks executing concurently in a batch.
In \texttt{BALS}, we use a thread block to solve a whole row block and deal with tiles of the row block through a loop fashion.
%Meanwhile, we implement the parallelized execution of row blocks in a batch.
The best performance of \texttt{BALS} involves the balance between the benefits due to column reuse and the computing costs because of increased rows in a row block.

\begin{figure}[!t]
\centering
\subfigure[ML10M(K20C)]{\label{fig_re_ml10M}\includegraphics[width=0.32\textwidth]{Data/ml10m_re.eps}}
\subfigure[ML20M(K20C)]{\label{fig_re_ml20M}\includegraphics[width=0.32\textwidth]{Data/ml20m_re.eps}} 
\subfigure[NTFX(K20C)]{\label{fig_re_ntfx}\includegraphics[width=0.32\textwidth]{Data/ntfx_re.eps}} 
\subfigure[YHR1(K20C)]{\label{fig_re_yhR1}\includegraphics[width=0.32\textwidth]{Data/yhr1_re.eps}} 
\subfigure[SLS(K20C)]{\label{fig_re_sls}\includegraphics[width=0.32\textwidth]{Data/sls_re.eps}}
\subfigure[RUCI(K20C)]{\label{fig_re_rucci}\includegraphics[width=0.32\textwidth]{Data/rucci_re.eps}}
\subfigure[ML10M(TITAN X)]{\label{fig_re_ml10M_p}\includegraphics[width=0.32\textwidth]{Data/ml10m_re_pascal.eps}}
\subfigure[ML20M(TITAN X)]{\label{fig_ml20M_re_p}\includegraphics[width=0.32\textwidth]{Data/ml20m_re_pascal.eps}}
\subfigure[NTFX(TITAN X)]{\label{fig_ntfx_re_p}\includegraphics[width=0.32\textwidth]{Data/ntfx_re_pascal.eps}}
\subfigure[YHR1(TITAN X)]{\label{fig_re_yhR1_p}\includegraphics[width=0.32\textwidth]{Data/yhr1_re_pascal.eps}} 
\subfigure[SLS(TITAN X)]{\label{fig_re_sls_p}\includegraphics[width=0.32\textwidth]{Data/sls_re_pascal.eps}}
\subfigure[RUCI(TITAN X)]{\label{fig_re_rucci_p}\includegraphics[width=0.32\textwidth]{Data/rucci_re_pascal.eps}} 
\caption{The performance impact over xblock and yblock and performance comparison between \texttt{BALS} and \texttt{Gates} after data reordering on various datasets, where $f$=32, nb=16, kb=8, DRbat=1.} 
\label{fig_results_re_xblock}
\end{figure}

Figure~\ref{fig_results_xblock} and Figure~\ref{fig_results_re_xblock} show the performance impact of xblock and yblock on various datasets before data reordering and after data reordering respectively. Meanwhile, we also make a performance comparison with the state-of-the-art implementation \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} (labeled in the colorbar of heatmaps).
We can observe that \texttt{BALS} generally outperforms \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} on various datasets and platforms. 
\texttt{BALS} presents the slightly gradual performance changes along with the augment of xblock and yblock and the best performance of \texttt{BALS} of datasets almost cluster in the left and upper corner of the heatmaps. 
The phenomenon enlightens us that how we choose the optimal parameters of \texttt{BALS} for given datasets in the future.
Also, we notice that there exit the dramatic performance changes in \texttt{Movielens 20M} and \texttt{YahooMusic R1} when xblock ranges from 256 to 1280, which is due to the special data distribution of these datasets.
The first few rows of the two datasets have so little non-zero elements that the data reuse technique cannot play a role in calculation.
%We achieve maximum 43.96\% performance improvement over \texttt{YahooMusic R1}, 29.84\% over \texttt{Sls}, 27.48\% over \texttt{Rucci}, 17.52\% over \texttt{Movielens 20M}, 9.22\% over \texttt{Netflix} and 1.25\% over \texttt{Movielens 10M} on K20C. 

\begin{figure}[!t]
\centering
\subfigure[Before reordering]{\label{fig_before}\includegraphics[width=0.45\textwidth]{Data/pfm_impro.eps}}
\subfigure[After reordering]{\label{fig_after}\includegraphics[width=0.45\textwidth]{Data/pfm_impro2.eps}} 
\caption{The performance improvement of \texttt{BALS} when compared with \texttt{Gates} before and after using data reordering technique over various datasets, where $f$=32.} 
\label{fig_performance_improvement}
\end{figure}

%Compared with performance before reordering,  presents the \texttt{BALS} performance of datasets  and also make a performance comparison with \texttt{Gates}.
%After data reordering, the performance of $YY^{T}$ using \texttt{BALS} achieves 9.38\% performance improvement over \texttt{Movielens 10M}, 53.43\% over \texttt{Movielens 20M}, 9.77\% over \texttt{Netflix} and 4.96\% over \texttt{YahooMusic R1} than \texttt{Gates}.
Figure~\ref{fig_performance_improvement} shows the performance improvement on the basis of \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15}.

\subsection{Performance Impact of Data Reordering}
We analyse the performance change of data reordering technique by comparing between figure~\ref{fig_results_xblock} and figure~\ref{fig_results_re_xblock}. 
For example, 
\texttt{Movielens 10M} achieves best performance at xblock=1280, yblock=384 on K20C before reordering, while at xblock=256, yblock=384 after reordering. 
The maximum performance are achieved for \texttt{Movielens 20M} at xblock=2816, yblock=384 on K20C before reordering and at xblock=768, yblock=384 after reordering. 
%\texttt{Netflix} achieves the same best performance at xblock=1280, yblock=384 before and after reordering. 
%Similarly, \texttt{YahooMusic R1} achieves best performance at xblock=5376, yblock=384 and xblock=768, yblock=384 after data reordering. 
These results demonstrate that \texttt{BALS} achieving the optimal performance needs more rows in a row block to take full advantage of benefits of column reuse before reordering than after.
In the same time, data reordering technique brings the performance enhancements, increasing by 23.68\% on K20C and 19.87\% on TITAN X over \texttt{Movielens 10M}, by 11.72\% on K20C and 11.92 \% on TITAN X over \texttt{Movielens 20M}, by 9.77\% on K20C and 8.96\% on TITAN X over \texttt{Netflix} and by 6.26\% on K20C and 5.87\% on TITAN X over \texttt{YahooMusic R1} on K20C.

However, we observe that there is no performance improvement in \texttt{Sls} and \texttt{Rucci} datasets, which are chosen from florida sparse matrix collection that arise in real applications. 
We analyze the two matrix shape and find that the shape of matrices are more structured and not particularlly scattered when compared with the other four matrices that specially used for recommender systems.  

%\begin{figure}[!t]
%\centering
%\subfigure[ML10M]{\label{fig_YBLOCK_ml10m}\includegraphics[width=0.49\textwidth]{Data/results_YBLOCK_ml10m.eps}}
%\subfigure[ML20M]{\label{fig_YBLOCK_ml20m}\includegraphics[width=0.49\textwidth]{Data/results_YBLOCK_ml20m.eps}}
%\subfigure[NTFX]{\label{fig_YBLOCK_ntfx}\includegraphics[width=0.49\textwidth]{Data/results_YBLOCK_ntfx.eps}}
%\subfigure[YHR1]{\label{fig_YBLOCK_yhr1}\includegraphics[width=0.49\textwidth]{Data/results_YBLOCK_yhr1.eps}} 
%\caption{The performance impact over yblock and performance comparison of \texttt{BALS} and \texttt{Gates} before and after reordering, where $f$=32.} 
%\label{fig_results_yblock}
%\end{figure}


%In \texttt{BALS}, yblock denotes the column block size of $R$ matrix and it has an effect on column reuse times of a tile. when the xblock of a tile is fixed, column reuse times of the tile will increase with yblock increasing theoretically. However, compared with xblock, it will not influence the total column reuse times of a entire row block.

%Figure shows the the sensitivity results of yblock on four datasets before and after reordering and we make a performance comparison with \texttt{Gates}.We seperate datasets into two groups: large datasets (\texttt{Netflix}, \texttt{Yahoomusic R1}) and small datasets (\texttt{Movielens 10M}, \texttt{Movielens 20M}).The tuning parameters in this part are listed in Table~\ref{tbl_parametersofBALS}.

%From figure, we observe that the execution time of $YY^{T}$ generally declines when yblock increases and achieves significant performance improvements after data reordering on all datasets.Also, the performance of \texttt{BALS} on three datasets all outperforms \texttt{Gates} in the range of 192 to 384 no matter before and after reordering, which represents that demolishing load of redundant columns data actually saves execution time.In addition, the execution time of \texttt{BALS} on small datasets like Movielens 10M runs faster than \texttt{Gates} when the column block size exceeds 328 before reordering and 256 after reordering. 

%\begin{table}[!h]
%\caption{Tuning Parameters of BALS (YBLOCK)}
%\label{tbl_parametersofBALS}
%\begin{center}
%\scalebox{0.62}{
%\begin{tabular}{|l|c|c|c|c|c|c|c|c|} \hline
%Parameter & ML10M & ML20M & NTFX & YHR1 & ML10M\_reorder & ML20M\_reorder & NTFX\_reorder & YHR1\_reorder \\ \hline
%xblock    & 800   & 2816  & 2000 & 6144 &      500       &   768      & 2100         & 2048         \\ \hline
%batch     & 4X    & 4X    & 12X  & 4X   & 7X             &6X          & 12X          & 4X    \\ \hline
%nb        & 16    & 16    & 16   & 16   & 16             & 16         & 16           & 16    \\ \hline
%kb        & 8     & 8     & 8    & 8    & 8              & 8          & 8            & 8     \\ \hline
%DRbat & 1     & 1     & 1    & 1    & 1              & 1          & 1            & 1     \\ \hline
%\end{tabular}}
%\end{center}
%\end{table}

\begin{figure}[!t]
\centering
\subfigure[datasets 1(K20C)]{\label{fig_p1batch_ml10M}\includegraphics[width=0.49\textwidth]{Data/results_P1BATCH_ml10m.eps}}
\subfigure[datasets 1(TITAN X)]{\label{fig_p1batch_ml20M}\includegraphics[width=0.49\textwidth]{Data/results_P1BATCH_ml20m.eps}} 
\subfigure[datasets 2(K20C)]{\label{fig_p1batch_ntfx}\includegraphics[width=0.49\textwidth]{Data/results_P1BATCH_ntfx.eps}} 
\subfigure[datasets 2(TITAN X)]{\label{fig_p1batch_yhR1}\includegraphics[width=0.49\textwidth]{Data/results_P1BATCH_yhr1.eps}} 
\caption{The performance impact over DRbat and comparison with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} before and after reordering over two platforms, where $f$=32, $nb$=16, $xblock$ and $yblock$ exploit the best parameter.} 
\label{fig_p1batch}
\end{figure}

\subsection{Performance Impact of DRbat}
In \texttt{BALS}, DRbat denotes the batch number of using \textit{data reuse} technique, which influences the performance directly. 
A batch includes many row blocks.
Data reuse technique does not always improve performance if there is not enough columns reuse in specific rowblock.
We sort all batches according to the total column reuse times accumulated by rowblocks and exploit \textit{data reuse} technique on top DRbat batches in order to make the best of benefits of columns reuse.
%However, there is no fixed number for DRbat because of different data distribution of each sparse dataset.

Figure~\ref{fig_p1batch} shows the performance impact of DRbat ranging from 1 to 10 before and after data reordering, where $f$=32, $nb$=16.
%The tuning parameters of each dataset in \texttt{BALS} are listed in the bottom of figures, including xblock, yblock and batch.
%The tuning parameters of each dataset in \texttt{BALS} are listed in Table~\ref{tbl_parameters}.
We can observe that in most cases, our implementation scales linearly up to 10 batches no matter before and after data reordering.
For example, we can obeserve that the execution time of \texttt{BALS} is same as \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} when DRbat equals 8 on YahooMusic R1 before and after reordering respectively.
Also, \texttt{BALS} achieves the same performance with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} when DRbat=3 on Netflix and Movielens 20M before reordering.
After row reordering, \texttt{BALS} achieves the same performance with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} when DRbat=7 on Netflix and DRbat=9 on Movielens 20M.
The results illustrate that \texttt{BALS} have more data reuse after data reordering and can take full advantage of benefits of data reuse as a result of data clustering at the upper part of sparse matrix.

%\begin{table}[!h]
%\caption{Tuning Parameters of \texttt{BALS} for DRbat}
%\label{tbl_parameters}
%\begin{center}
%\scalebox{0.62}{
%\begin{tabular}{|l|c|c|c|c|c|c|c|c|} \hline
%Parameter & ML10M & ML20M & NTFX & YHR1 & ML10M\_reorder & ML20M\_reorder & NTFX\_reorder & YHR1\_reorder \\ \hline
%xblock    & 1280   & 2816  & 1280 & 6144 &      500       &   256      & 1280         & 2048         \\ \hline
%yblock	  & 384   & 384   & 384  & 384  & 384            & 384        & 384          & 384   \\ \hline
%batch     & 4X    & 4X    & 12X  & 4X   & 7X             &6X          & 16X          & 4X    \\ \hline
%nb        & 16    & 16    & 16   & 16   & 16             & 16         & 16           & 16    \\ \hline
%kb        & 8     & 8     & 8    & 8    & 8              & 8          & 8            & 8     \\ \hline
%\end{tabular}}
%\end{center}
%\end{table}

\subsection{Performance Impact of Feature Space Size}
\begin{figure}[!t]
\centering
\subfigure[datasets 1(K20C)]{\label{fig_f1}\includegraphics[width=0.49\textwidth]{Data/feature.eps}}
\subfigure[datasets 1(TITAN X)]{\label{fig_f3}\includegraphics[width=0.49\textwidth]{Data/feature3.eps}}
\subfigure[datasets 2(K20C)]{\label{fig_f2}\includegraphics[width=0.49\textwidth]{Data/feature2.eps}} 
\subfigure[datasets 2(TITAN X)]{\label{fig_f4}\includegraphics[width=0.49\textwidth]{Data/feature4.eps}}
\caption{The performance impact of \texttt{BALS} and performance comparison with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} over different feature space size ranging from 8 to 96.} 
\label{fig_f}
\end{figure}

Figure~\ref{fig_f} shows the performance impact over different feature space size ranging from 8 to 96 on K20C and performance comparison with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15}.
We exploit the same task partitioning parameters and threads configuration with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} to guarantee the fairness of comparison.
For example when $f$=96, we set $nb$=48, $dx$=$dy$=8.
We can find that when $f$ is small, \texttt{BALS} achieves nearly the same performance with \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} on \texttt{Movielens 10M}.
While when $f$ grow larger, the performance gaps become more wider.
It is obvious that \texttt{BALS} generally outperforms \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} over different feature space size and achieves upto 1.98$\times$ and 3.14$\times$ speedup than \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} on K20C and TITAN X Pascal respectively.


